## Cytoscape Dijkstra

Link: https://puziopl.gitlab.io/cytoscapedijkstra/

Project based on my "CozyIndependencies" app. The only difference is that instead of calculating five different graph values, a user can choose two different nodes, and the app will find the most optimal path between them, based on edge weights. 

To perform finding the optimal path, right click a node, select "Find path", and then do the same on another node.
To change edge weight, right click it and select "Change weight".
