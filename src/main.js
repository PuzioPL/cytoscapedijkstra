/*
import Vue from 'vue'
//import App from './App.vue'
import { app } from './app'
//import router from './router'

Vue.config.productionTip = false

app.$mount('#app')
*/


import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VModal from 'vue-js-modal'
import './css/site.css'
import { FontAwesomeIcon } from './icons'

Vue.component('icon', FontAwesomeIcon)
Vue.config.productionTip = false

Vue.use(VModal)

new Vue({
  router,
  VModal,
  render: h => h(App)
}).$mount('#app')