//import CounterExample from 'components/counter-example'
import FetchData from '@/components/fetch-data'
import HomePage from '@/components/home-page'
import About from '@/components/about'
import Graph from '@/components/graph'

export const routes = [
    { name: 'home', path: '/cytoscapedijkstra', component: HomePage, display: 'Home', icon: 'home', visible: true },
    //{ name: 'counter', path: '/counter', component: CounterExample, display: 'Counter', icon: 'graduation-cap' },
    { name: 'fetch-data', path: '/cytoscapedijkstra/fetch-data', component: FetchData, display: 'Help', icon: 'question', visible: true },
    { name: 'graph', path: '/cytoscapedijkstra/graph', component: Graph, display: 'Graph', icon: 'pen', visible: true },
    { name: 'about', path: '/cytoscapedijkstra/about', component: About, display: 'About project', icon: 'info', visible: false },
    { name: 'graph_cutted', path: '/cytoscapedijkstra/graph_cutted', component: Graph, display: 'Graph', icon: 'pen', visible: true },
]
